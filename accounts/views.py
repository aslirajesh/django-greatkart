from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Account
from django.contrib.auth.models import auth
from django.utils.safestring import mark_safe
# verification email
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes
from django.core.mail import EmailMessage
from django.http import Http404
from django.contrib.auth.decorators import user_passes_test
from common.tokens import accountActivation_token_generator, passwordReset_token_generator



# Create your views here.

def anonymous_required(function=None, redirect_url=None):
    def _warper(request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(redirect_url)
        return function(request, *args, **kwargs)
    return _warper
        

@anonymous_required
def register(request):
    if request.method == 'POST':
        first_name  = request.POST['first_name']
        last_name   = request.POST['last_name']
        email       = request.POST['email']
        gender      = request.POST['gender']
        city        = request.POST['city']
        country     = request.POST['country']
        password    = request.POST['password']
        confirm_password    = request.POST['confirm_password']

        received_data = {
            'first_name' : first_name,
            'last_name' : last_name,
            'email' : email,
            'gender' : gender,
            'city' : city,
            'country' : country,
            'password' : password,
            'confirm_password' : confirm_password
        }
    
        if password != confirm_password:
            messages.error(request, 'Password do not match')
            return render(request, 'accounts/register.html', received_data)
        elif Account.objects.filter(email = email).exists():
            messages.error(request, mark_safe("Email exist, Please try <a href='/accounts/login'>Login</a>"))
            return render(request, 'accounts/register.html', received_data)
        else:
            User = Account.objects.create_user(
                first_name = first_name,
                last_name = last_name,
                username = email,
                email = email,
                password = password,)
            User.gender = gender
            User.city = city
            User.country = country
            User.save()

            #User ACTIVATION

            current_site = get_current_site(request)
            mail_subject = "Account activation Link for Greatkart"
            message = render_to_string('accounts/acc_activation_email.html',{
                'user':User,
                'domain':current_site.domain,
                'uid':urlsafe_base64_encode(force_bytes(User.pk)),         
                'token': accountActivation_token_generator.make_token(User),
            })
            email_message = EmailMessage(mail_subject, message, to=[email])
            # Disabling email verification for now
            # email_message.send()                
            return redirect('/accounts/login/?command=verification&email='+email)  
    else:
        return render(request, 'accounts/register.html')

@anonymous_required
def login(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']

        received_data = {
            'email' : email,
            'password' : password
        }
	
	
        if Account.objects.filter(email = email).exists():
            user = Account.objects.get(email = email)
            if user.check_password(password):
                if user.is_active:
                    auth.login(request, user)
                    return redirect('home')
                else:
                    messages.error(request, 'Account is not active')
                    return render(request, 'accounts/login.html', received_data)
            else:
                messages.error(request, 'Invalid Email or Password')
                return render(request, 'accounts/login.html', received_data)
        else:
            messages.error(request, 'Invalid Email or Password')
            return render(request, 'accounts/login.html', received_data)
    else:
        return render(request, 'accounts/login.html')
        
@login_required
def logout(request):
    if request.user.is_authenticated:
        auth.logout(request)
        messages.success(request, 'Logged Out')
        return redirect('login')
    else:
        messages.error(request, 'You are not logged in')
        return redirect('login')

def activate(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = Account.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, Account.DoesNotExist):
        user = None

    if user is not None and accountActivation_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        messages.success(request, 'Account Activated Successfully')
        return redirect('login')
    else:
        messages.error(request, 'Activation link is invalid')
        return redirect('login')


@login_required
def dashboard(request):
    return render(request, 'accounts/dashboard.html')


@anonymous_required
def forgotPassword(request):
    if request.method == 'POST':
        email = request.POST['email']
        if Account.objects.filter(email = email).exists():
            user = Account.objects.get(email = email)
            current_site = get_current_site(request)
            mail_subject = "Password Reset Link for Greatkart"
            message = render_to_string('accounts/password_reset_email.html',{
                'user':user,
                'domain':current_site.domain,
                'uid':urlsafe_base64_encode(force_bytes(user.pk)),         
                'token': passwordReset_token_generator.make_token(user),
            })
            email_message = EmailMessage(mail_subject, message, to=[email])
            #email_message.send()
            print(message)
            messages.success(request, 'Password reset link has been sent to your email')
            return redirect('login')
        else:
            messages.error(request, "Account doesn't exist")
            return render(request, 'accounts/forgotPassword.html')
    else:
        return render(request, 'accounts/forgotPassword.html')


@anonymous_required
def resetPasswordValidate(request, uidb64, token):
    try:
        uid = urlsafe_base64_decode(uidb64).decode()
        user = Account.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, Account.DoesNotExist):
        user = None

    if user is not None and passwordReset_token_generator.check_token(user, token):
        if user.is_active == False:
            user.is_active = True
            user.save()


        response = redirect('reset_password')
        response.set_cookie('reset_password_token', uidb64 + '-' + passwordReset_token_generator.make_token(user), max_age=300)
        return response
    else:
        messages.error(request, 'Password Reset link is invalid')
        return redirect('login')
    
@anonymous_required
def resetPassword(request):   
    try:
        reset_pass_cookie = request.COOKIES['reset_password_token']
        uidb64 = reset_pass_cookie.split('-')[0]
        token = '-'.join(reset_pass_cookie.split('-')[1:])
        uid = urlsafe_base64_decode(uidb64).decode()
        user = Account.objects.get(pk=uid)
    except(KeyError, TypeError, ValueError, OverflowError, Account.DoesNotExist):
        user = None

    if user is not None and passwordReset_token_generator.check_token(user, token):
        if request.method == 'POST':    
            password = request.POST['password']
            confirm_password = request.POST['confirm_password']
            if password != confirm_password:
                messages.error(request, 'Password do not match')
                return redirect('reset_password')
            else:
                user.set_password(password)
                user.save()
                messages.success(request, 'Password set successfully')
                response =  redirect('login')
                response.delete_cookie('reset_password_token')
                return response
        else:
            response = render(request, 'accounts/resetPassword.html')
            response.set_cookie('reset_password_token', uidb64 + '-' + passwordReset_token_generator.make_token(user), max_age=300)
            return response
    else:
        raise Http404